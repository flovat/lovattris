﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GamePlayController : MonoBehaviour,GameManager
{
    /*Questa classe gestirà tutte le attività di gioco, da parte del client.*/
    //Gestisco la struttura della tabella mediante un dizionario, in modo da accedere con complessità 1 a qualsiasi cella cliccata dal giocatore.
    private Dictionary<int,GameObject> AllCells;
    private static GamePlayController singleton = null;

    public Transform StartPointTable;
    public Transform ParentTable;
    public Text ToastText;
    public Text ToastTextErrors;
    public GameObject BannerEndGame;


    //Il primo a fare la mossa è il giocatore 1, impostato momentaneamente.
    private GameData.TYPE_PLAYER CURRENT_ROUND = GameData.TYPE_PLAYER.CLIENT;
    private Player PLAYER_CLIENT = null;
    private Player PLAYER_SERVER = null;

    //Attributi per controllare la tabella.
    private bool TableFilled = false;
    private bool EndGame = false;

    private void Awake()
    {
        if (singleton == null) singleton = this;
        //Istanzio i giocatori della partita.
        this.PLAYER_CLIENT = new Player(0, GameData.TYPE_PLAYER.CLIENT);
        this.PLAYER_SERVER = new Player(1, GameData.TYPE_PLAYER.SERVER);
    }

    public static GamePlayController getSingleton() { return singleton; }

    void Start()
    {
        BannerEndGame.SetActive(false);
        buildTable();
        StartCoroutine(ToastTextCoroutine());
    }

    void Update()
    {
        
    }

    //Metodo per la generazione della table di gioco.
    //Genero SIZE_TABLE celle nuove.

    //Metodo che mi stampa a video la tabella.
    private void buildTable()
    {
        Vector3 tmpPosition = new Vector3(StartPointTable.position.x,StartPointTable.position.y, StartPointTable.position.z);
        this.AllCells = new Dictionary<int, GameObject>();
        int tmpID = 0;
        for (int i = 0; i < GameData.SIZE_TABLE; i++) { 
            for (int j = 0; j < GameData.SIZE_TABLE; j++)
            {
                tmpPosition = new Vector3(StartPointTable.position.x + (GameData.DISTANCE_BTW_CELLS * i), StartPointTable.position.y - (GameData.DISTANCE_BTW_CELLS * j), StartPointTable.position.z);

                //GameObject tmpGM = (GameObject) Instantiate(Resources.Load<GameObject>(GameData.PATH_CELL_GM),tmpPosition,Quaternion.identity) as GameObject;
                GameObject tmpGM = (GameObject)Instantiate(Resources.Load<GameObject>(GameData.PATH_CELL_GM)) as GameObject;

                tmpGM.transform.SetParent(ParentTable,false);
                //Debug.Log(tmpGM+" "+ tmpPosition);
                tmpGM.transform.position = tmpPosition;
                tmpGM.GetComponent<Cell>().setID(tmpID);
                tmpGM.GetComponent<Cell>().setGameObject(tmpGM);
                tmpGM.GetComponent<Cell>().setPlayer(null);
                //Aggiungo la cella al dizionario.
                AllCells.Add(tmpID, tmpGM);
                //gm.transform.parent = ParentTable;
                tmpID++;
            }
        }
    }
    //Controllo se ci sono celle disponibili.
    private bool checkTable()
    {
        bool check = true;
        foreach(KeyValuePair<int,GameObject> obj in AllCells)
        {
            if (obj.Value.GetComponent<Cell>().isEmpty())
            {
                check = false;
                break;
            }
        }
        this.TableFilled = check;
        return this.TableFilled;
    }
    //Controllo sulla vittoria da parte di un giocatore. Richiamato alla fine di ogni mossa.
    //Se il controllo non va a buon termine allora si passa il turno.
    public void controlWin()
    {
        if (!checkPlayerWin())
        {
            //Finite le celle (senza nessun vincitore), mostro il pannello gameOver.
            if (checkTable())
            {
                BannerEndGame.SetActive(true);
                BannerEndGame.GetComponentInChildren<Text>().text = "GAME OVER";
                this.EndGame = true;
            }
            //Passo il turno.
            if (!EndGame)
                GamePlayController.getSingleton().changePlayerRound();
        }
        else
        {
            //Abbiamo un vincitore.
            BannerEndGame.SetActive(true);
            BannerEndGame.GetComponentInChildren<Text>().text = (this.CURRENT_ROUND == GameData.TYPE_PLAYER.CLIENT)? "WINNER!!" : "YOU LOSE.";
            this.EndGame = true;
        }
    }

    //Controllo la vittoria per uno specifico giocatore, in base a chi ha effettuato l'ultima mossa.
    //DA MODIFICARE!!!
    private bool checkPlayerWin()
    {
        //Controllo sulle righe.
        for(int i = 0; i< GameData.SIZE_TABLE; i++)
        {
            int numCosecutiveCells = 0;
            for (int j = 0; j < GameData.SIZE_TABLE; j++)
            {
                int numCellToCheck = i + (GameData.SIZE_TABLE * j);
                Cell cellToCheck = this.getSpecificCell(numCellToCheck);
                //Se esiste l'emento successivo al primo, nella riga esamita, continuo a controllare.
                if (cellToCheck.getPlayer() != null)
                {
                    //if(this.CURRENT_ROUND == GameData.TYPE_PLAYER.CLIENT) Debug.Log("Cell : " + cellToCheck.getID() + " Player: " + cellToCheck.getPlayer().getTypePlayer());
                    if ((cellToCheck.getPlayer().getTypePlayer() == this.CURRENT_ROUND)) { numCosecutiveCells += 1; }
                }
            }
            //Se ci sono tante celle consecutive dello stesso giocatore, quant'è grande la tabella, allora abbiamo un tris.
            if(numCosecutiveCells == GameData.SIZE_TABLE){  return true;   }
        }
        //Controllo sulle Colonne.
        for (int i = 0; i < (GameData.SIZE_TABLE * GameData.SIZE_TABLE); i += GameData.SIZE_TABLE)
        {
            int numCosecutiveCells = 0;
            for (int j = i; j < (i + GameData.SIZE_TABLE); j++)
            {
                int numCellToCheck = j;
                Cell cellToCheck = this.getSpecificCell(numCellToCheck);
                //Se esiste l'emento successivo al primo, nella riga esamita, continuo a controllare.
                if (cellToCheck.getPlayer() != null)
                {
                    if ((cellToCheck.getPlayer().getTypePlayer() == this.CURRENT_ROUND)) { numCosecutiveCells += 1; }
                }
            }
            //Se ci sono tante celle consecutive dello stesso giocatore, quant'è grande la tabella, allora abbiamo un tris.
            if (numCosecutiveCells == GameData.SIZE_TABLE) { return true; }
        }
        //Controllo sulla diagonale principale
        int numCells = 0;
        for (int i = 0; i< (GameData.SIZE_TABLE * GameData.SIZE_TABLE); i +=(GameData.SIZE_TABLE + 1))
        {
            Cell cellToCheck = this.getSpecificCell(i);
            //Se esiste l'emento successivo al primo, nella riga esamita, continuo a controllare.
            if (cellToCheck.getPlayer() != null)
            {
                if ((cellToCheck.getPlayer().getTypePlayer() == this.CURRENT_ROUND)) { numCells += 1; }
            }
        }
        if (numCells == GameData.SIZE_TABLE) return true;

        //Controllo sulla diagonale secondaria
        numCells = 0;
        for (int i = (GameData.SIZE_TABLE * (GameData.SIZE_TABLE -1)); i > 0; i -= (GameData.SIZE_TABLE - 1))
        {
            Cell cellToCheck = this.getSpecificCell(i);
            //Se esiste l'emento successivo al primo, nella riga esamita, continuo a controllare.
            if (cellToCheck.getPlayer() != null)
            {
                if ((cellToCheck.getPlayer().getTypePlayer() == this.CURRENT_ROUND)) { numCells += 1; }
            }
        }
        if (numCells == GameData.SIZE_TABLE) return true;
        return false;
    }

    //coroutine per aggiornare il toast text
    private IEnumerator ToastTextCoroutine()
    {
        while (true)
        {
            if(this.CURRENT_ROUND == GameData.TYPE_PLAYER.CLIENT)
                GamePlayController.getSingleton().writeOnToast("Your turn.",Color.green);
            else
                GamePlayController.getSingleton().writeOnToast("Opponent turn.",Color.yellow);
            yield return new WaitForSeconds(0.25f);
        }
    }


    public bool isTableFilled() { return this.TableFilled; }
    public Dictionary<int,GameObject> getCells() { return this.AllCells; }
    public Cell getSpecificCell(int id) { return this.AllCells[id].GetComponent<Cell>(); }

    //Ritorno il giocatore che deve compiere la mossa.
    public Player getPlayerRound() { return (this.CURRENT_ROUND == GameData.TYPE_PLAYER.CLIENT) ? this.PLAYER_CLIENT : this.PLAYER_SERVER; }
    //Setto il giocatore che deve eseguire la mossa del prossimo round.
    public void changePlayerRound() {
        this.CURRENT_ROUND = (this.CURRENT_ROUND == GameData.TYPE_PLAYER.CLIENT) ? GameData.TYPE_PLAYER.SERVER : GameData.TYPE_PLAYER.CLIENT;
        //Lancio la coroutine per la scelta della mossa da parte dell'AI
        if (this.CURRENT_ROUND == GameData.TYPE_PLAYER.SERVER)
            StartCoroutine(AIController.SINGLETON.AICoroutine());
    }
    //Toast
    public void writeOnToastErrors(string aux){   this.ToastTextErrors.text = aux; Invoke("cleanToastErrors", 1f); }
    public void writeOnToast(string aux, Color color) { this.ToastText.text = aux; this.ToastText.color = color; }
    private void cleanToastErrors() { this.ToastTextErrors.text = "---"; }

    //Restart Level
    public void OnClickReloadLevel()
    {
        SceneManager.LoadScene("GameplayTris");
    }
    //Exit
    public void OnClickExitLevel()
    {
        SceneManager.LoadScene("MenuTris");
    }


    //Override

    void GameManager.SendMessage()
    {
        throw new System.NotImplementedException();
    }

    void GameManager.ReceiveMessage()
    {
        throw new System.NotImplementedException();
    }
}
