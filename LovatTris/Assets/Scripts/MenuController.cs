﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{

    //Restart Level
    public void OnClickSinglePlayer()
    {
        SceneManager.LoadScene("GameplayTris");
        PlayerPrefs.SetInt("MULTIPLAYER",0);
    }
    //Restart Level
    public void OnClickMultiplayer()
    {
        SceneManager.LoadScene("GameplayTris");
        PlayerPrefs.SetInt("MULTIPLAYER", 1);
    }
    //Exit
    public void OnClickExitLevel()
    {
        Application.Quit();
    }
}
