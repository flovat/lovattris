﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{

    //Questa classe gestirà l'AI che giocherà contro il giocatore.

    public static AIController SINGLETON = null;
    private Coroutine AI = null;

    private void Awake()
    {
        SINGLETON = this;
    }

    void Start()
    {
        if (AI != null) StopCoroutine(AI);
        AI = StartCoroutine(AICoroutine());
    }

    void Update()
    {
        
    }

    //Metodo utilizzato per far effettuare la mossa all'AI.
    public void doMove()
    {
        bool exit = false;
        do
        {
            //Controllo se ci sono posizioni libere sulla tabella.
            if (GamePlayController.getSingleton().isTableFilled()) { exit = true; GamePlayController.getSingleton().writeOnToastErrors("Game Over, table not empty.");  break; }

            int cellNumber = Random.Range(0, GameData.SIZE_TABLE * GameData.SIZE_TABLE);
            if (GamePlayController.getSingleton().getSpecificCell(cellNumber).isEmpty())
            {
                GamePlayController.getSingleton().getSpecificCell(cellNumber).OnClickButtonServer();
                exit = true;
            }
            
        } while (!exit);
    }

    //Coroutine che esegue le mosse dell'AI se siamo in modalità single player.
    public IEnumerator AICoroutine()
    { 
        yield return new WaitForSeconds(1f);
        if (GamePlayController.getSingleton().getPlayerRound().getTypePlayer() == GameData.TYPE_PLAYER.SERVER)
        {
            if (!GamePlayController.getSingleton().isTableFilled())
                doMove();
        }   
    }
}
