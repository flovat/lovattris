﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData
{

    /*Classe statica utile per gestire tutti i valori di default del gioco.*/

    public enum TYPE_PLAYER { NONE, CLIENT, SERVER };

    public static int SIZE_TABLE = 5;
    public static float DISTANCE_BTW_CELLS = 150f;

    public static string PATH_CELL_GM = "Prefab/Cell";

}
