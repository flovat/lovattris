﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player 
{
    /*Questa classe gestirà tutti gli attributi riguardanti il giocatore, utile per mantenere in locale i dati dei giocatori partecipanti alla partita.*/

    private int ID;
    private GameData.TYPE_PLAYER MY_TYPE_PLAYER;

    public Player(int id, GameData.TYPE_PLAYER typeP)
    {
        this.ID = id;
        this.MY_TYPE_PLAYER = typeP;
    }

    public GameData.TYPE_PLAYER getTypePlayer() { return MY_TYPE_PLAYER; }

}
