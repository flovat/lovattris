﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Interfaccia per la comunicazione con il server.

interface GameManager
{
    void SendMessage();
    void ReceiveMessage();
}
