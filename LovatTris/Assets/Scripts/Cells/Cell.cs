﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*La classe cella aiuterà a gestire l'inserimento di una nuova mossa e i controlli sui casi critici.*/

public class Cell: MonoBehaviour
{
    public int ID;
    public GameObject X_Icon;
    public GameObject O_Icon;

    private GameObject GAME_OBJ;
    //Questo metodo serve per checkare se la cella è già stata segnata da un giocatore.
    private Player MY_PLAYER;
    public int getID() { return this.ID; }
    public void setID(int id){this.ID = id;}
    public GameObject getGameObject() { return this.GAME_OBJ; }
    public void setGameObject(GameObject gm){ this.GAME_OBJ = gm;}
    public Player getPlayer() { return this.MY_PLAYER; }
    public void setPlayer(Player pl){this.MY_PLAYER = pl;}
    public bool isEmpty() { return ((this.MY_PLAYER == null) ? true : false); }

    private void Awake()
    {
        X_Icon.SetActive(false);
        O_Icon.SetActive(false);
    }

    void Start()
    {
    }

    void Update()
    {
        
    }

    //Metodo richiamato dal server, oppure dalla classe AI per visualizzare la mossa a runtime.
    public void OnClickButtonServer() {
        this.MY_PLAYER = GamePlayController.getSingleton().getPlayerRound();
        this.O_Icon.SetActive(true);
        //Controllo sulla vincita.
        GamePlayController.getSingleton().controlWin();
    }


    //CLick sulla casella da parte dell'utente.
    public void OnClickButton()
    {
        if (this.isEmpty())
        {
            //Controllo se il client ha il diritto di fare la mossa.
            if(GamePlayController.getSingleton().getPlayerRound().getTypePlayer() == GameData.TYPE_PLAYER.CLIENT)
            {
                this.MY_PLAYER = GamePlayController.getSingleton().getPlayerRound();
                X_Icon.SetActive(true);
                //Creare il pacchetto messagge con all'interno l'oggetto mossa, da inviare al server.
                // !!!
                //Controllo sulla vincita.
                GamePlayController.getSingleton().controlWin();
            }
            else{   GamePlayController.getSingleton().writeOnToastErrors(GamePlayController.getSingleton().getPlayerRound().getTypePlayer() + " : wait your turn.");    }
        }
        else{   GamePlayController.getSingleton().writeOnToastErrors(GamePlayController.getSingleton().getPlayerRound().getTypePlayer() + " : cell not empty.");    }
    }
}
